package fr.imie.controller;

import fr.imie.entity.Funding;
import fr.imie.entity.Project;
import fr.imie.entity.User;
import fr.imie.repository.FundingRepository;
import fr.imie.repository.ProjectRepository;
import fr.imie.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;

@Controller
@RequestMapping("/fundings")
public class FundingController {


    @Autowired
    private FundingRepository fundingRepository;
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<Funding> getAllFundings() {
        return fundingRepository.findAll();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Funding getFundingById(@PathVariable("id") Long id) {
        return fundingRepository.findOne(id);
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ModelAndView addFunding(Funding funding) {
        Project project = projectRepository.getOne(funding.getProject().getId());
        User user = userRepository.findByEmail(
                ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
        funding.setUser(user);
        fundingRepository.saveAndFlush(funding);

        return new ModelAndView(new RedirectView("/projects/" + project.getId()));
    }

    @RequestMapping(value = "/remove/{id}", method = RequestMethod.DELETE)
    public void removeProject(@PathVariable(name = "id") Long id) {
        fundingRepository.delete(id);
    }

    @RequestMapping(value = "/project/{id}", method = RequestMethod.GET)
    public ModelAndView getFundingByProject(@PathVariable(name = "id") Long id) {
        ModelAndView mav = new ModelAndView( "project/project-funding");
        mav.addObject("project", projectRepository.findOne(id));
        mav.addObject("fundings",fundingRepository.findByProjectId(id));
        int result = this.countAmount(id);
        mav.addObject("amountTotal", result);
        mav.addObject("projectid", id);
        return mav;
    }

    private int countAmount(Long id){
        int result = 0;
        for (int i = 0; i < fundingRepository.findByProjectId(id).size(); i++) {
            result += fundingRepository.findByProjectId(id).get(i).getAmount();
        }
        return result;
    }

}
