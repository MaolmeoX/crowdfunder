package fr.imie.controller;

import fr.imie.entity.Project;
import fr.imie.entity.User;
import fr.imie.repository.CategoryRepository;
import fr.imie.repository.FundingRepository;
import fr.imie.repository.ProjectRepository;
import fr.imie.repository.UserRepository;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import java.util.Date;
import java.util.List;

@Controller
@Scope("session")
@RequestMapping("/projects")
public class ProjectController {

    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private FundingRepository fundingRepository;

    @GetMapping(value = "/api/all", produces = "application/json")
    public List<Project> getAllProjects() {
        return projectRepository.findAll();
    }

    @GetMapping(value = "/all")
    public ModelAndView allProjects(HttpServletRequest req) {
        ModelAndView mav = new ModelAndView("project/all-projects");

        User user = userRepository.findByEmail(
                ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());

        mav.addObject("projects", projectRepository.findAll());
        mav.addObject("categories", categoryRepository.findAll());
        return mav;
    }

    @GetMapping(value = "/new")
    public ModelAndView newProject() {
        ModelAndView mav = new ModelAndView("project/new-project", "project", new Project());
        mav.addObject("categories", categoryRepository.findAll());
        return mav;
    }

    @PostMapping(value = "/new")
    public ModelAndView addProject(Project project) {
        java.sql.Date dateSQL = new java.sql.Date(new Date().getTime());
        project.setCreatedAt(dateSQL);
        User user = userRepository.findByEmail(
                ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
        project.setCreator(user);
        projectRepository.saveAndFlush(project);
        return new ModelAndView(new RedirectView("/projects/" + project.getId()));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ModelAndView getProjectById(@PathVariable("id") Long id) {
        ModelAndView mav = new ModelAndView("project/project-detail");
        if (projectRepository.findOne(id) != null) {
            mav.addObject("project", projectRepository.findOne(id));
            return mav;
        } else {
            return new ModelAndView(new RedirectView("/projects/all"));
        }
    }

    @RequestMapping(value = "/remove/{id}", method = RequestMethod.DELETE)
    public void removeProject(@PathVariable(name = "id") Long id) {
        projectRepository.delete(id);
    }

    @RequestMapping(value = "/category/{id}", method = RequestMethod.GET)
    public ModelAndView getProjectsByCategory(@PathVariable(name = "id") Long id) {
        ModelAndView mav = new ModelAndView("project/projects-by-category");
        if (id == 0) {
            mav.addObject("projects", projectRepository.findAll());
        } else {
            mav.addObject("projects", projectRepository.findByCategoryId(id));
        }
        return mav;
    }
}
