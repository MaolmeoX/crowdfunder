package fr.imie.controller;

import fr.imie.entity.User;
import fr.imie.repository.UserRepository;
import fr.imie.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;

@Controller
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserServiceImpl userService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView getHomepage(HttpServletRequest req) {
        if (req.getSession().getAttribute("email") == null) {
            return new ModelAndView(new RedirectView("/login"));
        } else {
            return new ModelAndView(new RedirectView("/projects/all"));
        }
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView getLogin(HttpServletRequest req) {
        if (req.getSession().getAttribute("email") != null) {
            return new ModelAndView(new RedirectView("/projects/all"));
        } else {
            return new ModelAndView("login");
        }
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public ModelAndView getLogout(HttpServletRequest req) {
        if (req.getSession().getAttribute("username") != null) {
            req.getSession().removeAttribute("username");
        }
        return new ModelAndView(new RedirectView("/login"));
    }

    @GetMapping(value = "/user/profil")
    public ModelAndView showProfil() {
        User user = userRepository.findByEmail(((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
        ModelAndView mav = new ModelAndView("user/edit-user");
        mav.addObject("user", user);
        return mav;
    }

    @PostMapping(value = "/user/profil")
    public ModelAndView editProfil(User user) {
        userService.editUser(user);
        ModelAndView mav = new ModelAndView("user/edit-user");
        mav.addObject("user", user);
        return mav;
    }

    @GetMapping(value = "/register")
    public ModelAndView getAccountCreation() {
        return new ModelAndView("user/new-user", "user", new User());
    }

    @PostMapping(value = "/register")
    public ModelAndView postAccountCreation(User user, @RequestParam(value = "password-conf") String passwordConf) {
        if (!user.getPassword().equals(passwordConf)) {
            user.setPassword(null);
            return new ModelAndView("user/new-user", "user", user);
        }
        user.setActive(true);
        userService.saveUser(user);
        return new ModelAndView(new RedirectView("/login"));
    }

}
