package fr.imie.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table
public class Funding {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Float amount;

    @ManyToOne
    @JoinColumn (name = "project_id")
    private Project project;

    @ManyToOne
    @JoinColumn (name = "user_id")
    private User user;

    private Date fundingDate;

    public Funding() {
    }

    public Funding(Float amount, Project project, User user, Date fundingDate) {
        this.amount = amount;
        this.project = project;
        this.user = user;
        this.fundingDate = fundingDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getFundingDate() {
        return fundingDate;
    }

    public void setFundingDate(Date fundingDate) {
        this.fundingDate = fundingDate;
    }
}
