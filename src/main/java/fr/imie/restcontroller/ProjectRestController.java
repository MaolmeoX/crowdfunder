package fr.imie.restcontroller;

import fr.imie.entity.Project;
import fr.imie.entity.User;
import fr.imie.repository.CategoryRepository;
import fr.imie.repository.ProjectRepository;
import fr.imie.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api/projects")
public class ProjectRestController {

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @GetMapping(value = "/all", produces = "application/json")
    public List<Project> getAllProjects() {
        return projectRepository.findAll();
    }

    @GetMapping(value = "/{id}", produces = "application/json")
    public Project getOneProject(@PathVariable Long id) {
        return projectRepository.findOne(id);
    }

    @PostMapping(value = "/new")
    public ResponseEntity newProject(@RequestBody Project project) {
        if (categoryRepository.findOne(project.getCategory().getId()) == null) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body("category not found");
        } else {
            project.setCategory(categoryRepository.findOne(project.getCategory().getId()));
            java.sql.Date dateSQL = new java.sql.Date(new Date().getTime());
            project.setCreatedAt(dateSQL);
            User user = userRepository.findOne(Long.valueOf(1));
            project.setCreator(user);
            Project projectDb = projectRepository.saveAndFlush(project);
            return new ResponseEntity<>(projectDb, HttpStatus.OK);
        }
    }

}
