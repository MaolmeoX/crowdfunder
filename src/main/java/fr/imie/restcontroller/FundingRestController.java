package fr.imie.restcontroller;

import fr.imie.entity.Funding;
import fr.imie.entity.Project;
import fr.imie.entity.User;
import fr.imie.repository.CategoryRepository;
import fr.imie.repository.FundingRepository;
import fr.imie.repository.ProjectRepository;
import fr.imie.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api/fundings")
public class FundingRestController {

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private FundingRepository fundingRepository;

    @PostMapping(value = "/new")
    public ResponseEntity newFunding(@RequestBody Funding funding) {
        if (projectRepository.findOne(funding.getProject().getId()) == null) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body("project not found");
        } else if (userRepository.findOne(funding.getUser().getId()) == null) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body("user not found");
        }
        else {
            java.sql.Date dateSQL = new java.sql.Date(new Date().getTime());
            funding.setFundingDate(dateSQL);
            Funding fundingDb = fundingRepository.saveAndFlush(funding);
            return new ResponseEntity<>(fundingDb, HttpStatus.OK);
        }
    }
}
