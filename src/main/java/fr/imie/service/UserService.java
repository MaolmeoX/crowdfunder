package fr.imie.service;


import fr.imie.entity.User;

public interface UserService {
    User findUserByEmail(String email);

    void saveUser(User user);

    void editUser(User user);
}