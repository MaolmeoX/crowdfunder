$(document).ready(function() {
    $('select').material_select();
    $(".button-collapse").sideNav();

});
$("select").on("change", function (event) {
    $.ajax({
        method: "GET",
        url: "category/" + $("select").val(),
        dataType: "html"
    }).done(function( msg ) {
        $('.projectByCategory').html(msg);
    }).fail(function(msg) {
        //alert( msg.url + " error " + msg.responseText );
    });
});
